//TEMP v3 Material-UI, in Material v4 should be removed
import { install } from "@material-ui/styles";

import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";

import { createMuiTheme } from "@material-ui/core/styles";
import { ThemeProvider } from "@material-ui/styles";

import Layout from "./Components/Layout";
//import SignUp from './Components/Pages/Auth/SignUp';

import store from "./store";

import "./index.css";

//TEMP v3 Material-UI, in Material v4 should be removed
install();

const app = document.getElementById("app");
const theme = createMuiTheme({
  palette: {
    common: {
      black: "#000",
      white: "#fff"
    },
    background: {
      paper: "rgba(255, 255, 255, 1)",
      default: "rgba(255, 255, 255, 1)"
    },
    primary: {
      light: "rgba(74, 101, 114, 1)",
      main: "rgba(52, 73, 85, 1)",
      dark: "rgba(36, 47, 53, 1)",
      contrastText: "rgba(255, 255, 255, 1)"
    },
    secondary: {
      light: "rgba(248, 187, 85, 1)",
      main: "rgba(248, 170, 51, 1)",
      dark: "rgba(248, 147, 9, 1)",
      contrastText: "#fff"
    },
    error: {
      light: "#e57373",
      main: "#f44336",
      dark: "#d32f2f",
      contrastText: "#fff"
    },
    text: {
      primary: "rgba(0, 0, 0, 0.87)",
      secondary: "rgba(0, 0, 0, 0.54)",
      disabled: "rgba(0, 0, 0, 0.38)",
      hint: "rgba(0, 0, 0, 0.38)"
    }
  },
  typography: { useNextVariants: true }
});

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Provider store={store}>
      <Layout />
    </Provider>
  </ThemeProvider>,
  app
);
