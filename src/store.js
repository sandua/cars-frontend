import { applyMiddleware, createStore, compose } from 'redux';

//import logger from 'redux-logger';
import thunk from 'redux-thunk';
import promise from 'redux-promise-middleware'

import reducer from './Reducers';

const errorLogger = (store) => (next) => (action) => {
  try {
    next(action);
  } catch(e) {
    console.error("OMG error catched by middleware: ", e);
  }
}

//Add bellow think, logger, errorLogger (for redux states and logger in console)
const middleware = [promise(), thunk, errorLogger];

const composeEnhancers =
  typeof window === 'object' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
      // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
    }) : compose;

const enhancer = composeEnhancers(
  applyMiddleware(...middleware),
  // other store enhancers if any
);

export default createStore(reducer, enhancer);

// store.dispatch({
//   type: "FETCH_USER",
//   payload: axios.get("http://rest.learncode.academy/api/western/users")
// })
