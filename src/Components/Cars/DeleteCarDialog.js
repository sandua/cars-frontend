import React from "react";

import { connect } from "react-redux";
import { deleteCar } from "../../Actions/carsActions";

import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { withStyles } from "@material-ui/core/styles";

const styles = theme => ({
  button: {
    margin: theme.spacing.unit
  }
});

class DeleteCarDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
    };

    this.handleClickOpen = this.handleClickOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.removeCar = this.removeCar.bind(this);
  }

  render() {
    const {classes, plateNo, car } = this.props;
    const {open} = this.state;

    const carName = car.make + " " + car.model;

    return (
      <React.Fragment>
        <Button variant="outlined" onClick={this.handleClickOpen}>
          <i className="material-icons">delete_forever</i>
        </Button>
        <Dialog
          open={open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">
            {"Delete car " + plateNo + " ?"}
          </DialogTitle>

          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              {"Permanently delete " +
                carName +
                " (" +
                plateNo +
                ") ?  You can't undo this."}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button
              variant="outlined"
              color="primary"
              onClick={this.handleClose}
              className={classes.button}
            >
              Cancel
            </Button>
            <Button
              variant="outlined"
              color="secondary"
              onClick={this.removeCar}
              className={classes.button}
            >
              Delete
            </Button>
          </DialogActions>
        </Dialog>
      </React.Fragment>
    );
  }

  handleClickOpen() {
    this.setState({ open: true });
  }

  handleClose() {
    this.setState({ open: false });
  }

  removeCar() {
    this.props.deleteCar(this.props.car.id);
    this.handleClose();
  }
}

//Not needed, just a demo
const mapStateToProps = state => {
  return {
    cars: state.cars.cars
  };
};

const mapDispatchToProps = dispatch => {
  return {
    deleteCar: carId => {
      dispatch(deleteCar(carId));
    }
  };
};

export default withStyles(styles)(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(DeleteCarDialog)
);
