import React from 'react';
import Car from './Car';

export default class CarsList extends React.Component {

  render() {
    const mappedCars = this.props.cars.map(car => <Car key={car.id} car={car}/>);

    if(!this.props.cars.length) {
      return (
        <div className="d-flex justify-content-center">
        <div className="sk-folding-cube">
          <div className="sk-cube1 sk-cube"></div>
          <div className="sk-cube2 sk-cube"></div>
          <div className="sk-cube4 sk-cube"></div>
          <div className="sk-cube3 sk-cube"></div>
        </div>
        </div>
      );
    }

    return (
      <table className="table table-hover table-responsive-md">
      <thead>
        <tr>
          <th scope="col">Plate Number</th>
          <th scope="col">Car</th>
          <th scope="col">Color</th>
          <th scope="col">Details</th>
          <th scope="col">Status</th>
          <th scope="col">Actions</th>
        </tr>
      </thead>
      <tbody>
          { mappedCars }
      </tbody>
      </table>
    );
  }
}
