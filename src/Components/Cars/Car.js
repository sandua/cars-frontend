import React from "react";

import Button from "@material-ui/core/Button";

import DeleteCarDialog from "./DeleteCarDialog";

export default class Car extends React.Component {
  constructor(props) {
    super(props);

    this.formatPlate.bind(this);
    this.calc.bind(this);
    this.getStyle.bind(this);
    this.getDanger.bind(this);
  }

  formatPlate(plateNumber) {
    if (plateNumber.length < 7) {
      return plateNumber;
    }

    let numberMatch = plateNumber.match(/\d+/);

    if (numberMatch.length <= 0) {
      return plateNumber;
    }

    let index = plateNumber.indexOf(numberMatch[0]);

    return (
      plateNumber.substring(0, index) +
      " " +
      numberMatch[0] +
      " " +
      plateNumber.substring(index + numberMatch[0].length)
    );
  }

  calc() {
    //Regula de 3 simpla - TODO IMPLEMENT
    //Calculate in percent (100 days, remaining 15 < 30 days, remaining 5)
    const MAX_VALUE = 100;
    return Math.floor(Math.random() * Math.floor(MAX_VALUE));
  }

  getStyle(value) {
    return {
      width: value + "%"
    };
  }

  getDanger(value, maxValue) {
    let diff = maxValue - value + " zile";
    let percent = Math.floor((value * 100) / maxValue) + "%";

    if (value > 90) {
      return {
        time: diff,
        cn: "progress-bar bg-danger",
        percent: percent
      };
    } else if (value > 75) {
      return {
        time: diff,
        cn: "progress-bar bg-warning",
        percent: percent
      };
    }

    return {
      time: diff,
      cn: "progress-bar bg-success",
      percent: ""
    };
  }

  render() {
    const car = this.props.car;
    const MAX_VALUE = 110;
    const plateNo = this.formatPlate(car.plate_number);
    const calc = this.calc();
    const status = this.getDanger(calc, MAX_VALUE);

    return (
      <tr>
        <td>{plateNo}</td>
        <td>
          {car.make} {car.model}
        </td>
        <td>{car.color}</td>
        <td>
          ({car.year}/{car.fuel_type})
        </td>
        <td>
          <div>Expira in {status.time}</div>
          <div className="progress">
            <div
              className={status.cn}
              style={this.getStyle(calc)}
              role="progressbar"
            >
              {status.percent}
            </div>
          </div>
        </td>
        <td>
          <Button variant="outlined" href={"/showcar/" + car.id}>
            <i className="material-icons">directions_car</i>
          </Button>
          <Button variant="outlined" href={"/editcar/" + car.id}>
            <i className="material-icons">edit</i>
          </Button>
          <Button variant="outlined" href="#top">
            <i className="material-icons">alarm</i>
          </Button>
          <DeleteCarDialog plateNo={plateNo} car={car}/>
        </td>
      </tr>
    );
  }
}
