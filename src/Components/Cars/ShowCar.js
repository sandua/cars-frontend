import React, { Component } from "react";

export default class ShowCar extends Component {
  componentDidMount() {}

  render() {
    return (
      <div className="card">
        <h5 className="card-header">CAR ID:{this.props.match.params.car_id}</h5>
        <div className="card-body">
          <h5 className="card-title">Special car treatment</h5>
          <p className="card-text">
            With supporting text below as a natural lead-in to additional
            content.
          </p>
          <a
            href={"/editcar/" + this.props.match.params.car_id}
            className="btn btn-primary"
          >
            Edit
          </a>
        </div>
      </div>
    );
  }
}
