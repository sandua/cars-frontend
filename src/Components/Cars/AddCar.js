import React from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import NativeSelect from "@material-ui/core/NativeSelect";

import HelpCard from "../Widgets/Cards/HelpCard";

function AddCar(props) {
  const [values, setValues] = React.useState({
    plateNo: "",
    make: "",
    model: "",
    type: "",
    color: "",
    fuel_type: "",
    year: ""
  });

  const [errors, setErrors] = React.useState({
    plateNo: "",
    make: "",
    model: "",
    type: "",
    color: "",
    fuel_type: "",
    year: ""
  });

  const handleChange = event => {
    setValues({ ...values, [event.target.name]: event.target.value });
  };

  const handleSubmit = event => {
    event.preventDefault();
    const { value, field } = event.target;

    switch (field) {
      case "plateNo":
        if (value.length < 0) triggerError(field, "Plate number is required.");
        break;
      case "make":
        if (value.length < 0) triggerError(field, "Plate number is required.");
        break;
      case "model":
        if (value.length < 0) triggerError(field, "Plate number is required.");
        break;
    }

  };

  const triggerError = (field, message, resetValue = false) => {
    setErrors({ ...errors, [field]: message });
    if (resetValue == true) setValues({ ...values, [field]: "" });
  };

  const test = () => {
    console.log("BLUR >>>");
    console.log(values);
  };

  return (
    <React.Fragment>
      <div className="container mt-4 mb-4">
        <div className="row">
          <h4>Add car</h4>
        </div>
        <div className="row">
          here error map
        </div>
        <div className="row">
          <div className="col-12 col-md-9">
            <form autoComplete="on">
              <div className="row">
                <div className="col-12 col-sm-4 mt-3">
                  <TextField
                    required
                    name="plateNo"
                    label="Plate number"
                    fullWidth
                    onChange={handleChange}
                    value={values.plateNo}
                    onBlur={test}
                  />
                </div>
              </div>
              <div className="row">
                <div className="col-12 col-sm-4 mt-3">
                  <TextField
                    required
                    name="make"
                    label="Make"
                    fullWidth
                    onChange={handleChange}
                    value={values.make}
                    onBlur={test}
                  />
                </div>
                <div className="col-12 col-sm-4 mt-3">
                  <TextField
                    required
                    name="model"
                    label="Model"
                    fullWidth
                    onChange={handleChange}
                    value={values.model}
                    onBlur={test}
                  />
                </div>
                <div className="col-12 col-sm-4 mt-3">
                  <TextField
                    name="type"
                    label="Type"
                    fullWidth
                    onChange={handleChange}
                    value={values.type}
                    onBlur={test}
                  />
                </div>
              </div>
              <div className="row">
                <div className="col-12 col-sm-4 mt-3">
                  <TextField
                    name="color"
                    label="Color"
                    fullWidth
                    onChange={handleChange}
                    value={values.color}
                    onBlur={test}
                  />
                </div>
                <div className="col-12 col-sm-4 mt-3">
                  <FormControl fullWidth error={errors.fuel_type !== ""}>
                    <InputLabel htmlFor="name-native-error">Fuel</InputLabel>
                    <NativeSelect
                      value={values.fuel_type}
                      onChange={handleChange}
                      name="fuel_type"
                      input={<Input id="name-native-error" />}
                    >
                      <option value="" />
                      <option value="Diesel">Diesel</option>
                      <option value="Benzin">Benzin</option>
                      <option value="GPL">GPL</option>
                      <option value="Electric">Electric</option>
                    </NativeSelect>
                  </FormControl>
                </div>
                <div className="col-12 col-sm-4 mt-3">
                  <TextField
                    name="year"
                    label="Year"
                    fullWidth
                    onChange={handleChange}
                    value={values.year}
                    onBlur={test}
                  />
                </div>
              </div>
              <div className="row mt-4">
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                  onClick={handleSubmit}
                >
                  Add car
                </Button>
              </div>
            </form>
          </div>
          <div className="col-md-3 d-none d-md-block">
            <HelpCard />
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}

export default AddCar;
