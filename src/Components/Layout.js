import React from "react";
import { BrowserRouter } from "react-router-dom";

import Header from "./Header";
import Content from "./Content";
import Footer from "./Footer";

import { connect } from "react-redux";
import { fetchUser } from "../Actions/usersActions";
import { fetchCars } from "../Actions/carsActions";

class Layout extends React.Component {
  componentWillMount() {
    this.props.dispatch(fetchUser());

    if (this.props.cars.length === 0) {
      this.props.dispatch(fetchCars());
    }
  }

  // fetchProducts() {
  //   this.props.dispatch(fetchProducts());
  // }

  render() {
    const { user, cars } = this.props;

    // <button onClick={this.fetchProducts.bind(this)}>Load user</button>
    //
    // let mappedProducts = 'No products, sorry.';
    // if(products.length) {
    //   mappedProducts = products.map(product => <li key="{product.id}">{product.id} * {product.name} * {product.price}$</li>);
    // }

    return (
      <BrowserRouter>
        <React.Fragment>
          <Header user={user} />
          <Content cars={cars} user={user} />
          <Footer />
        </React.Fragment>
      </BrowserRouter>
    );
  }
}

export default connect(store => {
  return {
    user: store.users.user,
    products: store.products.products,
    cars: store.cars.cars
  };
})(Layout);
