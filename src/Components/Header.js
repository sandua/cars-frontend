import React from 'react';

import Navbar from './Widgets/Navbar';

export default class Header extends React.Component {
  render() {

    return (
        <React.Fragment>
          <Navbar user={this.props.user}/>
        </React.Fragment>
    );
  }
}
