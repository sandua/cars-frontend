import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { InputAdornment, withStyles } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import { Visibility, VisibilityOff } from '@material-ui/icons';

const styles = theme => ({
  passwordEye: {
    cursor: 'pointer',
    color: theme.palette.primary.main
  },
});

class PasswordInput extends Component {
  constructor(props) {
    super(props);

    this.state = {
      passwordIsMasked: true,
    };
  }

  togglePasswordMask = () => {
    this.setState(prevState => ({
      passwordIsMasked: !prevState.passwordIsMasked,
    }));
  };

  render() {
    const { classes, label } = this.props;
    const { passwordIsMasked } = this.state;

    return (
      <TextField type={passwordIsMasked ? 'password' : 'text'} label={label}
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
            <i {...this.props} className={classes.passwordEye} onClick={this.togglePasswordMask}>
                { passwordIsMasked ? <Visibility/> : <VisibilityOff/>}
            </i>
            </InputAdornment>
          ),
        }}
      />
    );
  }
}

PasswordInput.propTypes = {
  classes: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
};

export default withStyles(styles)(PasswordInput);
