import React from 'react';

export default class Status extends React.Component {
  render() {
    let user = this.props.user;

    if(user.id) {
      return <h1>Hello {user.name}!</h1>;
    }
    else {
      return <p>Not logged in</p>;
    }
  }
}
