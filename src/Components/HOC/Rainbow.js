import React from 'react';

const Rainbow = (WrappedComponent) => {

  const colors = ['text-primary', 'text-secondary', 'text-success', 'text-danger', 'text-warning', 'text-info'];
  const randomColor = colors[Math.floor(Math.random() * 5)];

  return (props) => {
    return (
      <div className={randomColor}>
        <WrappedComponent {...props}/>
      </div>
    )
  }
}

export default Rainbow;
