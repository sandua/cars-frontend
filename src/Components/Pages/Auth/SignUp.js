import React, { Component } from "react";
import PropTypes from "prop-types";

import { Link as RouterLink } from "react-router-dom";
import Link from "@material-ui/core/Link";

import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import FormControl from "@material-ui/core/FormControl";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import Paper from "@material-ui/core/Paper";
import Hidden from "@material-ui/core/Hidden";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";

import PasswordInput from "../../Auth/PasswordInput";
import PasswordInput2 from "../../Auth/PasswordInput2";

const styles = theme => ({
  main: {
    width: "auto",
    display: "block", // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: "auto",
      marginRight: "auto"
    }
  },
  spacer: {
    marginTop: theme.spacing.unit * 20
  },
  paper: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme
      .spacing.unit * 3}px`
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.primary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing.unit
  },
  submit: {
    marginTop: theme.spacing.unit * 3
  },
  typography: {
    marginTop: theme.spacing.unit * 3
  }
});

class SignUp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
      confirmPwd: ""
    };
  }

  onChange = event => {
    const { name, value } = event.target;

    this.setState({ [name]: value });
  };

  render() {
    const { email, password, confirmPwd } = this.state;
    const { classes } = this.props;
    const tosLabel =
      'I accept <a href="#top" target="_blank"> Terms of Service</a>';
    const label = (
      <div>
        <span>I accept the </span>
        <Link to={"/terms"}>terms of use</Link>
        <span> and </span>
        <Link to={"/privacy"}>privacy policy</Link>
      </div>
    );
    return (
      <main className={classes.main}>
        <Hidden only={["xs", "sm"]}>
          <div className={classes.spacer} />
        </Hidden>
        <CssBaseline />
        <Paper className={classes.paper}>
          <img
            src="http://img.excavatortimisoara.com/public/logo.png"
            alt="logo"
          />
          <Typography
            className={classes.typography}
            component="h1"
            variant="h5"
          >
            Sign up
          </Typography>
          <form className={classes.form}>
            <FormControl margin="normal" required fullWidth>
              <InputLabel htmlFor="email">Email</InputLabel>
              <Input
                id="email"
                name="email"
                onChange={this.onChange}
                autoComplete="email"
                autoFocus
              />
            </FormControl>
            <FormControl margin="normal" required fullWidth>
              <PasswordInput
                id="password"
                name="password"
                label="Password *"
                autoComplete="new-password"
                value={password}
                onChange={this.onChange}
              />
            </FormControl>
            <FormControl margin="normal" required fullWidth>
              <PasswordInput
                id="confirmPwd"
                name="confirmPwd"
                label="Repeat password *"
                autoComplete="new-password-confirm"
                value={password}
                onChange={this.onChange}
              />
            </FormControl>
            <FormControlLabel
              control={<Checkbox value="tos" color="primary" />}
              label={label}
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Sign Up
            </Button>
          </form>
        </Paper>
      </main>
    );
  }
}

SignUp.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(SignUp);
