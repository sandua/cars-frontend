import React from "react";

import JujuList from "../Widgets/Cards/JujuList";
import CarsList from "../Cars/CarsList";

export default class Home extends React.Component {
  render() {
    let cars = this.props.cars;

    return (
      <div className="container-fluid">
        <JujuList cars={cars} />
        <CarsList cars={cars} />
      </div>
    );
  }
}
