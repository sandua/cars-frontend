import React from 'react';

export default class NoMatch extends React.Component {
  render() {

    return (
      <div className="container text-center">
        <div><img src="https://i.giphy.com/media/kHHGpkPgvDp0YJLMG9/giphy.webp" alt="404 error img"/></div>
        <h1>404</h1>
      </div>
    )
  }
}
