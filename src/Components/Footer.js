import React from 'react';

export default class Footer extends React.Component {
  render() {
    const REACT_VERSION = React.version;

    return (
      <React.Fragment>
      <div className="container-fluid text-center">
        <p>Copyright (C) a great company. <a href="#bottom"> Terms and conditions </a></p>
        <p>React version: {REACT_VERSION}</p>
      </div>
      </React.Fragment>
    );
  }
}
