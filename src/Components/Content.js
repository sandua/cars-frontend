import React from "react";
import { Route, Switch } from "react-router-dom";

import AddCar from "./Cars/AddCar";
import EditCar from "./Cars/EditCar";
import ShowCar from "./Cars/ShowCar";

import About from "./Pages/About";
import Contact from "./Pages/Contact";
import Home from "./Pages/Home";

import NoMatch from "./Pages/Special/NoMatch";

export default class Content extends React.Component {
  render() {
    let { cars, user } = this.props;

    return (
      <div className="container-fluid">
        <Switch>
          <Route exact path="/" render={() => <Home cars={cars} />} />
          <Route path="/about" render={() => <About user={user} />} />
          <Route path="/contact" component={Contact} />
          <Route path="/addcar" component={AddCar} />
          <Route path="/editcar/:car_id" component={EditCar} />
          <Route path="/showcar/:car_id" component={ShowCar} />
          <Route component={NoMatch} />
        </Switch>
      </div>
    );
  }
}
