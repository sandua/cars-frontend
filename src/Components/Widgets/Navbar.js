import React from 'react';

import { NavLink, withRouter } from 'react-router-dom'

class Navbar extends React.Component {

  render() {
    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <a className="navbar-brand" href="/"><img src="http://img.excavatortimisoara.com/public/logo.png" alt="logo"/></a>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item active">
              <NavLink className="nav-link" to="/">Dashboard</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/about">About</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/contact">Contact</NavLink>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#top" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Dropdown
              </a>
              <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                <a className="dropdown-item" href="#top">Action</a>
                <a className="dropdown-item" href="#top">Another action</a>
                <div className="dropdown-divider"></div>
                <a className="dropdown-item" href="#top">Something else here</a>
              </div>
            </li>
            <li className="nav-item">
              <NavLink className="btn btn-outline-info d-flex" to="/addcar"><i className="material-icons">add</i>&nbsp;Add car</NavLink>
            </li>
          </ul>
          <button type="button" className="btn btn-outline-info d-flex"><i className="material-icons">person</i>&nbsp;{this.props.user.name}</button>
        </div>
      </nav>
    )
  }
}

export default withRouter(Navbar);
