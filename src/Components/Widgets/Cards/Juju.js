import React from "react";
import PropTypes from "prop-types";

import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";

const styles = theme => ({
  spacer: {
    marginTop: theme.spacing.unit * 20
  }
});

class Juju extends React.Component {
  constructor(props) {
    super(props);

    this.getStyle.bind(this);
  }

  getStyle(color) {
    return {
      backgroundColor: color,
      border: "1px solid " + color,
      color: "#FFFFFF"
    };
  }

  render() {
    let info = this.props.info;

    return (
      <div className="row Juju" style={this.getStyle(info.color)}>
        <div className="col-xs-3">
          <i className="material-icons icon"> {info.icon} </i>
        </div>
        <div className="col col-xs-9 text-right margin-top-7">
        <Typography
          className="details-top"
          component="h1"
          variant="h3"
        >
          {info.info}
        </Typography>
          <Typography
            className="details-bot"
            component="h1"
            variant="h5"
          >
            {info.text}
          </Typography>
        </div>
      </div>
    );
  }
}

Juju.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Juju);
