import React from "react";
import Juju from "./Juju";

import blue from '@material-ui/core/colors/blue';
import lightGreen from '@material-ui/core/colors/lightGreen';
import blueGrey from '@material-ui/core/colors/blueGrey';
import purple from '@material-ui/core/colors/purple';

export default class JujuList extends React.Component {

  constructor(props) {
    super(props);

    this.calculateTotalCars.bind(this);
    this.getInfo.bind(this);
  }

  render() {
    let cars = this.props.cars;

    return (
      <div className="row">
        <div className="col-lg-3 col-md-6">
          <Juju info={this.calculateTotalCars(cars)} />
        </div>
        <div className="col-lg-3 col-md-6">
          <Juju info={this.getInfo(1)} />
        </div>
        <div className="col-lg-3 col-md-6">
          <Juju info={this.getInfo(2)} />
        </div>
        <div className="col-lg-3 col-md-6">
          <Juju info={this.getInfo(3)} />
        </div>
      </div>
    );
  }

  calculateTotalCars(cars) {
    const SHADE = 400;

    return {
      info: this.props.cars.length,
      text: "Total cars",
      icon: "time_to_leave",
      color: blue[SHADE]
    };
  }

  getInfo(cars) {
    const SHADE = 400;

    if (cars === 1) {
      return {
        info: '23 days',
        text: "Next renewal",
        icon: "alarm",
        color: lightGreen[SHADE]
      };
    }
    if (cars === 2) {
      return {
        info: "TM 13 SND",
        text: "Next car for renewal",
        icon: "attach_money",
        color: blueGrey[SHADE]
      };
    }
    if (cars === 3) {
      return {
        info: 'TM 62 CDA',
        text: "Next service",
        icon: "build",
        color: purple[SHADE]
      };
    }
  }
}
