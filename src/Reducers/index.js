import { combineReducers } from 'redux'

import products from './productsReducer';
import users from './usersReducer';
import cars from './carsReducer';

export default combineReducers({
  products,
  users,
  cars
})
