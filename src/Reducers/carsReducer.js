const initialState = {
  cars: [],
  fetching: false,
  fetched: false,
  error: null
};

export default function carsReducer(state = initialState, { type, payload }) {
  switch (type) {
    case "FETCH_CARS_PENDING": {
      return { ...state, fetching: true};
    }
    case "FETCH_CARS_REJECTED": {
      return { ...state, fetching: false, error: payload};
    }
    case "FETCH_CARS_FULFILLED": {
      return {
        ...state,
        fetching: false,
        fetched: true,
        cars: payload,
      };
    }

    case "ADD_CAR": {
      return {...state,
        cars: {...state.cars, payload}
      };
    }

    case "UPDATE_CAR": {
      const { id } = payload;
      const newCars = [...state.cars];
      const carToUpdate = newCars.findIndex(product => product.id === id);
      newCars[carToUpdate] = payload;

      return {
        ...state,
        cars: newCars
      };
    }

    case "DELETE_CAR": {
      return {
        ...state,
        cars: state.cars.filter(car => car.id !== payload)
      }
    }

    case "DELETE_CAR_PENDING": {
      return { ...state, fetching: true};
    }
    case "DELETE_CAR_REJECTED": {
      return { ...state, fetching: false, error: payload};
    }
    case "DELETE_CAR_FULFILLED": {
      let newCars = [...state.cars];
      newCars = state.cars.filter(car => car.id !== parseInt(payload));

      return {
        ...state,
        fetching: false,
        fetched: true,
        cars: newCars
      };
    }

    default:
      return state;
  }
}
