const initialState = {
  products: [],
  fetching: false,
  fetched: false,
  error: null
};

export default function productsReducer(state = initialState, { type, payload }) {
  switch (type) {
    case "FETCH_PRODUCTS_PENDING": {
      return { ...state, fetching: true};
    }
    case "FETCH_PRODUCTS_REJECTED": {
      return { ...state, fetching: false, error: payload};
    }
    case "FETCH_PRODUCTS_FULFILLED": {
      return {
        ...state,
        fetching: false,
        fetched: true,
        products: payload,
      };
    }

    case "ADD_PRODUCT": {
      return {...state,
        products: {...state.products, payload}
      };
    }

    case "UPDATE_PRODUCT": {
      const { id } = payload;
      const newProducts = [...state.products];
      const productToUpdate = newProducts.findIndex(product => product.id === id);
      newProducts[productToUpdate] = payload;

      return {
        ...state,
        products: newProducts
      };
    }

    case "DELETE_PRODUCT": {
      return {
        ...state,
        products: state.products.filter(product => product.id !== payload)
      }
    }
    default:
      return state;
  }
}
