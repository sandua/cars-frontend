const initialState = {
  user: {
    id: null,
    name: null,
    email: null,
  },
  fetching: false,
  fetched: false,
  error: null
};

export default function usersReducer(state = initialState, { type, payload }) {
  switch (type) {
    case "FETCH_USER_PENDING": {
      return { ...state, fetching: true};
    }
    case "FETCH_USER_REJECTED": {
      return { ...state, fetching: false, error: payload};
    }
    case "FETCH_USER_FULFILLED": {
      return {
        ...state,
        fetching: false,
        fetched: true,
        user: payload
      };
    }

    case "SET_USER_NAME": {
      return {...state, name: payload};
    }

    case "SET_USER_AGE": {
      return {...state, age: payload};
    }

    default:
      {
        return state;
      }
  }
}
