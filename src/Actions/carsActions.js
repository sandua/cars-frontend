import axios from 'axios';

export function fetchCars() {
  return function(dispatch) {
    dispatch ({ type: "FETCH_CARS_PENDING" });

    axios.get("http://192.168.0.107/Cars/cars-backend/public/cars")
    .then((response) => {
      dispatch ({ type: "FETCH_CARS_FULFILLED", payload: response.data })
    })
    .catch((err) => {
      dispatch ({ type: "FETCH_CARS_REJECTED", payload: err })
    })
    }
}

export function deleteCar(carId) {
  return function(dispatch) {
    dispatch ({ type: "DELETE_CAR_PENDING" });

    axios.get("http://localhost/Cars/cars-backend/public/cars/delete/" + carId)
    .then((response) => {
      dispatch ({ type: "DELETE_CAR_FULFILLED", payload: response.data })
    })
    .catch((err) => {
      dispatch ({ type: "DELETE_CAR_REJECTED", payload: err })
    })
    }
}

export function addProduct(product) {
  return {
    type: "ADD_CARS",
    payload: {
      id: 3,
      name: "My third product",
      price: 90
    }
  }

  // {
  //   id: product.id,
  //   name: product.name,
  //   price: product.price
  // }
}

export function updateProduct(product) {
  return {
    type: "UPDATE_CAR",
    payload: product
  }
}

export function deleteProduct(productId) {
  return {
    type: "DELETE_CAR",
    payload: 1
  }
}
