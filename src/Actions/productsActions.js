export function fetchProducts() {
  return {
    type: "FETCH_PRODUCTS_FULFILLED",
    payload : [{
      id: 1,
      name: "My first product",
      price: 20
    }]
  }
}

export function addProduct(product) {
  return {
    type: "ADD_PRODUCT",
    payload: {
      id: 3,
      name: "My third product",
      price: 90
    }
  }

  // {
  //   id: product.id,
  //   name: product.name,
  //   price: product.price
  // }
}

export function updateProduct(product) {
  return {
    type: "UPDATE_PRODUCT",
    payload: product
  }
}

export function deleteProduct(productId) {
  return {
    type: "DELETE_PRODUCT",
    payload: 1
  }
}
